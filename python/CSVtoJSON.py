import os
import csv
import json

currentPath = os.getcwd()


def csv_to_json():
    data_dict = {}

    with open(currentPath + '/python/pages.csv', encoding='utf-8') as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler)

        for row in csv_reader:
            if row['page'] not in data_dict and row['page'] != "":
                data_dict[row['page']] = {}

            for attr in row:
                data_dict[row['page']][attr] = row[attr]

    with open(currentPath + '/src/data/pages.json', 'w', encoding='utf-8') as json_file_handler:
        json_file_handler.write(json.dumps(data_dict, indent=4))

    print('done!')


csv_to_json()
