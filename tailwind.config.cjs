/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'light-table': '#F7E6D5',
        'brown-paper': '#B18C65',
        'film-strip': 'rgb(127,70,58)'
      },
      screens: {
        'touch': {'raw': '(hover: none)'},
        'mobile-landscape': {'raw': '(orientation: landscape) and (max-width: 1024px)'}
      }
    }
  },
  plugins: []
};