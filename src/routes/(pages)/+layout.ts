import pages from '$data/pages.json'

/** @type {import('@sveltejs/kit').Load} */
export const load = (({ route, params }) => {
  const key = params.page || route.id.split('/')[2] || 'base'
  const gallery = params.gallery || ''
  const image = params.image || ''
  return {
    key,
    gallery,
    image,
    pages
  };
})