// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	// interface Error {}
	// interface Locals {}
	// interface PageData {}
	// interface Platform {}
}

interface Image {
  src: string
  aspect: "portrait" | "landscape" | "square"
  alt: string
  text?: string
}

interface Pages {[key: string]: Page}

interface Gallery {
  page: string
  title: string
  text?: string
  place?: string
  date?: string | number
  quant?: string | number
  transparent: number[]
  images: Image[][]
}

interface RoutesData {
  key: string
  gallery: string
  image: string
  pages: Pages
}

interface Page {
  page?: string
  title: string
  text: string
  place?: string
  date?: string
  quant?: string | number
  images: Image[][]
  galleries: Gallery[]
}
