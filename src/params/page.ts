import type { ParamMatcher } from '@sveltejs/kit';
import pages from '$data/pages.json'

export const match = ((param) => {
  return param in pages;
}) satisfies ParamMatcher;