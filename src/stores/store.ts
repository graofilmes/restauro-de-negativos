import { writable } from 'svelte/store';

export const lockNavigation = writable(false);
export const openEnvelope = writable(false);
